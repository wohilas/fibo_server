package main

import "math/big"

func fibonacci() func() int {
	a, b := 0, 1
	return func() (ret int) {
		ret, a, b = a, b, a+b
		return
	}
}

func fiboBig(n int) *big.Int {
	if n <= 1 {
		return big.NewInt(int64(n))
	}

	var n2, n1 = big.NewInt(0), big.NewInt(1)

	for i := 1; i < n; i++ {
		n2.Add(n2, n1)
		n1, n2 = n2, n1
	}

	return n1
}

func fibo(n int) int {
	var lastValue int
    fib := fibonacci()
    for i := 0; i <= n; i++ {
        lastValue = fib()
    }
	return lastValue
}
