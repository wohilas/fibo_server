package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

var (
	hostURL = "localhost:8060"
)

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	// only one handler; needs map if more handlers/routes in use
	// like map[url] = handler;
	fiboHandler(rr, req)
	return rr
}

// typicalCall: helper function that makes request and returns response
func typicalCall(num int, method string) *http.Response {
	req := httptest.NewRequest(method, fmt.Sprintf("%s/%d", hostURL, num), nil)
	rr := executeRequest(req)
	resp := rr.Result()
	return resp
}

func TestWrongMethod(t *testing.T) {
	resp := typicalCall(5, "GET")
	if resp.StatusCode != 200 {
		t.Fatalf("Got unacceptable code value: %d", resp.StatusCode)
	}
	body, _ := ioutil.ReadAll(resp.Body)
	if string(body) != "Only POST method currently supported" {
		t.Fatalf("Got unacceptable response text: %s", string(body))
	}
}
