package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// fiboHandler handles requests
func fiboHandler(w http.ResponseWriter, req *http.Request) {

	if req.Method == "POST" {
		// skip slash
		urlValue := strings.TrimPrefix(req.URL.Path, "/")

		num, err := strconv.Atoi(urlValue)
		if err != nil {
			http.Error(w, "you should provide integer number",
				http.StatusBadRequest)
			return
		}

        var result interface{}
        if num >= 93{
            result = fiboBig(num)
        }else{
		    result = fibo(num)
        }
		io.WriteString(w, fmt.Sprintf("%d", result))
		return
	}
	msg := "Only POST method currently supported"
	io.WriteString(w, msg)
	return
}

func main() {
	http.HandleFunc("/", fiboHandler)
	if err := http.ListenAndServe(":8060", nil); err != nil {
		log.Fatal(err)
	}
}
